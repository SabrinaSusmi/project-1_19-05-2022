var key = "8c5c6b19c18ba3691db9c367010dd504";
var units = "metric";
var countries;
var loc = document.getElementById("loc");
var temp = document.getElementById("temp");
var min_temp = document.getElementById("min_temp");
var max_temp = document.getElementById("max_temp");
var wind = document.getElementById("wind");
var humid = document.getElementById("humid");
var description = document.getElementById("description");
var icon = document.getElementById("icon_img");
var unit_d = document.getElementById("unit_d");


//// get current location
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    window.alert("Geolocation is not supported by this browser.");
  }
}

function showPosition(position) {
  //console.log(position.coords.longitude, position.coords.latitude);
  if(position) {
    getWeather(position.coords.longitude, position.coords.latitude);
    getForecast(position.coords.latitude, position.coords.longitude);
  } else {
    window.alert("Location access denied!");
  }
}


///fetch weather
async function getWeather(longitude, latitude) {
  fetch(
    `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${key}&units=${units}`
  )
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      console.log(data);
      loc.innerHTML = `<i class="fa-solid fa-sun-dust"></i>` + data.name;
      temp.innerHTML = data.main.temp;
      wind.innerHTML = data.wind.speed + " m/s";
      humid.innerHTML = data.main.humidity + "%";
      description.innerHTML = data.weather[0].description;
      min_temp.innerHTML = data.main.temp_min;
      max_temp.innerHTML = data.main.temp_max;
      icon.innerHTML = `<img src="http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png">`;
    })
    .catch((err) => {
      console.log(err);
    });
}


///fetch forecast
async function getForecast(lat, lon) {
  fetch(
    `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${key}&units=${units}`
  )
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      //console.log(data);
      const dt_time = [];
      const temp_f = [];
      data.list.forEach((dt) => {
        if (dt_time.length < 16) {
          dt_time.push(dt.dt_txt);
          temp_f.push(dt.main.temp);
        }
      });

      //console.log(dt_time);
      createChart(dt_time, temp_f);
    })
    .catch((err) => {
      console.log(err);
    });
  }

  getLocation();

  
  
  
  ////////////////// Choose Location ///////////////////

  ///fetch country list from api
  async function getCountries() {
    const response = fetch(
      "https://countriesnow.space/api/v0.1/countries/positions"
    );

    // if (response.status != 200) {
    //   throw new Error(`Something went wrong, status code: ${response.status}`);
    // }

    const data = (await response).json();
    //console.log(data);

    const countrylist = data.then((result) => result.data);
    //console.log(countrylist)

    return countrylist;
  }

  ///fetch lon, lat of a location
  async function getWeatherOfCity(cityName, SC, CC) {
    fetch(
      `https://api.openweathermap.org/geo/1.0/direct?q=${cityName},${SC},${CC}&limit=5&appid=${key}`
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        console.log(data[0].lat);
        getWeather(data[0].lon, data[0].lat);
      })
      .catch((err) => {
        console.log(err);
      });
  }


  //Get country list
  const countrylist = document.querySelector("#countries");

  document.addEventListener("DOMContentLoaded", async () => {
    const countries = await getCountries();

    let countryOptions = "";
    //console.log(countries);
    if (countries) {
      countryOptions += `<option value="">Select Country</option>`;
      countries.forEach((country) => {
        countryOptions += `<option label="${country.name}" value="${country.iso2}">${country.name}</option>`;
      });

      countrylist.innerHTML = countryOptions;
    } else {
      window.alert("Couldn't fetch countries!");
    }
  });
  
  
  //select country, get state
  const statelist = document.querySelector("#states");

  countrylist.addEventListener("change", async () => {
    fetch(
      `https://api.countrystatecity.in/v1/countries/${countrylist.value}/states`,
      {
        headers: {
          "X-CSCAPI-KEY":
            "WFEwaWtHWDJ2cTNmQjNwZk1DZmZ1dHcwVjBIVzk5VmVWUVBwQk4wZg==",
        },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        const states = data;
        let stateOptions = "";
        if (states) {
          stateOptions += `<option value="">Select State</option>`;
          states.forEach((state) => {
            stateOptions += `<option value="${state.iso2}">${state.name}</option>`;
          });

          statelist.innerHTML = stateOptions;
        }
        else {
          window.alert("States not available for this country")
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });


  //select state, get city
  const citylist = document.querySelector("#cities");

  statelist.addEventListener("change", async () => {
    fetch(
      `https://api.countrystatecity.in/v1/countries/${countrylist.value}/states/${statelist.value}/cities`,
      {
        headers: {
          "X-CSCAPI-KEY":
            "WFEwaWtHWDJ2cTNmQjNwZk1DZmZ1dHcwVjBIVzk5VmVWUVBwQk4wZg==",
        },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        const cities = data;
        //console.log(cities)
        let cityOptions = "";
        if (cities) {
          cityOptions += `<option value="">Select City</option>`;
          cities.forEach((city) => {
            cityOptions += `<option value="${city.name}">${city.name}</option>`;
          });

          citylist.innerHTML = cityOptions;
        } else {
          window.alert("cities not available")
        }

        citylist.addEventListener("change", async () => {
          getWeatherOfCity(citylist.value, statelist.value, countrylist.value);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });

  ////////////// Forecast chart /////////////////

function createChart(dt_time, temp_f) {
  const ctx = document.getElementById("myChart").getContext("2d");
  // console.log(dt_time, temp_f)
  const myChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: dt_time,
      datasets: [
        {
          label: "Temperature (celcius)",
          data: temp_f,
          borderColor: [
            "rgba(255, 99, 132, 1)",
            "rgba(54, 162, 235, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
            "rgba(255, 159, 64, 1)",
          ],
          borderWidth: 2,
        },
      ],
    },
  });

  
  const unitSelectorCel = document.querySelector("#cel");
  const unitSelectorFar = document.querySelector("#far");


  unitSelectorCel.addEventListener("click", () => {
    console.log(unit_d.innerText)
    if(unit_d.innerText!='°C') {
      temp.innerHTML = parseFloat(((temp.innerText-32)*5)/9).toFixed(2);
      min_temp.innerHTML = parseFloat(((min_temp.innerText-32)*5)/9).toFixed(2);
      max_temp.innerHTML = parseFloat(((max_temp.innerText-32)*5)/9).toFixed(2);
      unit_d.innerHTML = '°C';
    }
  });

  unitSelectorFar.addEventListener("click", () => {
    if(unit_d.innerText!='°F') {
      console.log(temp.innerText);
      temp.innerHTML = parseFloat((temp.innerText*9/5)+32).toFixed(2);
      min_temp.innerHTML = parseFloat((min_temp.innerText*9/5)+32).toFixed(2);
      max_temp.innerHTML = parseFloat((max_temp.innerText*9/5)+32).toFixed(2);
      unit_d.innerHTML = '°F'
    }
  });

  //   var timestamp = 1607110465663
  // var date = new Date(timestamp);

  // console.log("Date: "+date.getDate()+
  //           "/"+(date.getMonth()+1)+
  //           "/"+date.getFullYear()+
  //           " "+date.getHours()+
  //           ":"+date.getMinutes()+
  //           ":"+date.getSeconds());
}
